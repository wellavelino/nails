package nails.com.br.nails.br.com.nails.business;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import nails.com.br.nails.br.com.nails.DAO.UserDAO;
import nails.com.br.nails.br.com.nails.bean.User;
import nails.com.br.nails.br.com.nails.connection.DBSQLite;

/**
 * Created by Wellington on 31/07/2015.
 */
public class RegisterUser implements IRegisterUser {

    private User user;
    private CursorFactory cursorFactory;
    private SQLiteOpenHelper sqLiteOpenHelper;
    private Context context;


    public RegisterUser(Context context, CursorFactory cursorFactory) {
        this.context = context;
        sqLiteOpenHelper = DBSQLite.getInstance(this.context, this.cursorFactory);
    }





    @Override
    public void registerUser(User user) throws Exception {
        UserDAO userDAO = new UserDAO(sqLiteOpenHelper);
        userDAO.registerUser(user);

    }

    @Override
    public boolean recoverUser(String email) throws SQLException, IllegalAccessException, NullPointerException, Exception {
        UserDAO userDAO = new UserDAO(sqLiteOpenHelper);

        return userDAO.isRegistered(email);
    }
}
