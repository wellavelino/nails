package nails.com.br.nails.br.com.nails.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


import nails.com.br.nails.br.com.nails.controller.FragmentLogin;
import nails.com.br.nails.br.com.nails.controller.RegisterFragment;


public class FragmentLoginAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public FragmentLoginAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    RegisterFragment registerFragment = new RegisterFragment();
                    return registerFragment;
                case 1:
                    FragmentLogin fragmentLogin = new FragmentLogin();
                    return fragmentLogin;

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }
