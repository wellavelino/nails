package nails.com.br.nails.br.com.nails.controller;

import android.support.design.widget.TabLayout;

import android.support.v4.view.ViewPager;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.view.MenuItem;



import nails.com.br.nails.R;
import nails.com.br.nails.br.com.nails.adapter.FragmentLoginAdapter;


public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        TabLayout tabLayout = (TabLayout) findViewById(R.id.slidingTabs);
        tabLayout.addTab(tabLayout.newTab().setText("Registro"));
        tabLayout.addTab(tabLayout.newTab().setText("Log in"));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        final FragmentLoginAdapter adapterLogin = new FragmentLoginAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapterLogin);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
