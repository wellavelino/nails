package nails.com.br.nails.br.com.nails.connection;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import nails.com.br.nails.br.com.nails.business.Constants;


public class DBSQLite extends SQLiteOpenHelper implements Constants {
    private static final String NOME_BANCO_SQLITE = "nails.db";
    private static final String NOME_DIRETORIO_SQLITE = "/databases/";
    private static final int VERSAO_BANCO_SQLITE = 1;
    private static DBSQLite instance = null;

    public DBSQLite(Context context, CursorFactory factory) {
        super(context, context.getFilesDir().getParent() + NOME_DIRETORIO_SQLITE + NOME_BANCO_SQLITE, null, VERSAO_BANCO_SQLITE);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        if (database.isOpen()) {
            database.execSQL("create table if not exists tb_register(id_user INTEGER PRIMARY KEY AUTOINCREMENT, email TEXT, userName TEXT, password TEXT);");


        } else {
            Log.i(LOGS, "ERRO! DataBase is closed" + NOME_BANCO_SQLITE);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public static DBSQLite getInstance(Context context, CursorFactory factory) {
        if (instance == null) {
            instance = new DBSQLite(context, factory);
        }
        return instance;
    }
}
