package nails.com.br.nails.br.com.nails.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import nails.com.br.nails.br.com.nails.bean.User;

/**
 * Created by Wellington on 31/07/2015.
 */
public class UserDAO {

    private SQLiteOpenHelper sqLiteOpenHelper;

    public UserDAO(SQLiteOpenHelper sqLiteOpenHelper) {
        super();
        this.sqLiteOpenHelper = sqLiteOpenHelper;
    }

    public void registerUser(User user) throws IllegalAccessException {

        SQLiteDatabase database = sqLiteOpenHelper.getWritableDatabase();

        if (database.isOpen()) {
            try {
                ContentValues contentValues = new ContentValues();

                contentValues.put("email", user.getEmail());
                contentValues.put("userName", user.getUserName());
                contentValues.put("password", user.getPassword());


                database.insert("tb_register", null, contentValues);
            } finally {
                if (database != null) {
                    database.close();
                }
            }
        } else {
            throw new IllegalAccessException("O banco de dados nao esta aberto");
        }
    }

    public boolean isRegistered(String email) throws IllegalAccessException {

        SQLiteDatabase database = sqLiteOpenHelper.getReadableDatabase();

        boolean exist = false;
        String[] selectionArgs =
                {email.toString().trim()};

        if (database.isOpen()) {
            Cursor cursor = null;

            try {
                String sql = "select * from tb_register where email = ?";

                cursor = database.rawQuery(sql, selectionArgs);

                cursor.moveToFirst();

                if (cursor.getCount() > 0) {

                    while (!cursor.isAfterLast()) {
                        exist = true;
                        break;
                    }
                }
            } finally {
                if (database != null) {
                    database.close();
                }

                if (cursor != null) {
                    cursor.close();
                }
            }
        } else {
            throw new IllegalAccessException("O banco de dados nao esta aberto");
        }

        return exist;
    }
}


