package nails.com.br.nails.br.com.nails.bean;

/**
 * Created by Wellington on 30/07/2015.
 */
public class User {

    private String id_usuario;
    private String fullName;
    private String email;
    private String userName;
    private String password;
    private Access access;

    /**
     * verificar como  usar imagens *
     */


    public User() {
    }

    public User(String fullName, String email, String userName, String password) {
        this.fullName = fullName;
        this.email = email;
        this.userName = userName;
        this.password = password;
    }

    public String getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(String id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Access getAccess() {
        return access;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAccess(Access access) {
        this.access = access;
    }

    @Override
    public String toString() {
        return "User{" +
                "id_usuario='" + id_usuario + '\'' +
                ", fullName='" + fullName + '\'' +
                ", email='" + email + '\'' +
                ", access=" + access +
                '}';
    }
}
