package nails.com.br.nails.br.com.nails.business;

import android.database.SQLException;

import nails.com.br.nails.br.com.nails.bean.User;

/**
 * Created by Wellington on 31/07/2015.
 */
public interface IRegisterUser {



    public abstract void registerUser(User user) throws SQLException, IllegalAccessException, NullPointerException, Exception;

    public abstract boolean recoverUser(String email)throws SQLException, IllegalAccessException, NullPointerException, Exception;

}
