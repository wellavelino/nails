package nails.com.br.nails.br.com.nails.bean;

/**
 * Created by Wellington on 30/07/2015.
 */
public class Access {

    private String userName;
    private String password;

    public Access() {
    }

    public Access(String userName,String password)
    {
        super();
        this.userName = userName;
        this.password = password;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Access{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
