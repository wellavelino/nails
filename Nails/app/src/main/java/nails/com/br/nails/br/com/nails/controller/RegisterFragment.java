package nails.com.br.nails.br.com.nails.controller;

import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import nails.com.br.nails.R;
import nails.com.br.nails.br.com.nails.business.RegisterUser;

/**
 * Created by Wellington on 29/07/2015.
 */
public class RegisterFragment extends Fragment {

    private EditText txtEmail;
    private EditText txtUserName;
    private EditText txtPasswordRegister;
    private Button btnRegister;
    private AlertDialog alertDialogEmpty;
    private CursorFactory cursorFactory;
    private Fragment context;
    private RegisterUser registerUser;
    private boolean isRegistered;
    FragmentManager fm;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.username_fragment, container, false);

        btnRegister= (Button) view.findViewById(R.id.testeFrag);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RegisterFragment f1 = new RegisterFragment();
                FragmentTransaction transaction = fm.beginTransaction();
                transaction.addToBackStack(null);

                transaction.replace(R.id.fragmentContainer, f1);
                transaction.commit();

            }
        });
        return view;
    }


   /** @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.register_fragment, container, false);

        txtEmail = (EditText) view.findViewById(R.id.txtEmail);
        txtUserName = (EditText) view.findViewById(R.id.txtUserName);
        txtPasswordRegister = (EditText) view.findViewById(R.id.txtPasswordRegister);

        btnRegister = (Button) view.findViewById(R.id.btnRegister);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                System.out.println("vai entrar?");
                if (txtEmail.equals("") || txtUserName.equals("") || txtPasswordRegister.equals("")) {
                    System.out.println("primeiro");
                    createDialog();
                    return;
                }
                if ((!txtEmail.equals("")) || (!txtUserName.equals("")) || (!txtPasswordRegister.equals("")))
                    try {
                        String email = String.valueOf(txtEmail);
                        System.out.println("DENTRO DO IF");

                        isRegistered = registerUser.recoverUser(email);
                        System.out.println("DENTRO DO IF" + isRegistered);

                        if (isRegistered == true) {
                            System.out.println("existeeeF");
                            createDialogExist();
                            return;
                        } else {
                            System.out.println("registrou");
                            registerUser();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                Intent intent = new Intent(getActivity(), SplashScreenHomeActivity.class);
                System.out.println("FORAAAA DO IF");
                startActivity(intent);
            }
        });
        return view;

    }

    private void registerUser() {

        User user = new User();
        System.out.println("elseeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
        user.setEmail(txtEmail.getText().toString());
        user.setUserName(txtUserName.getText().toString());
        user.setPassword(txtPasswordRegister.getText().toString());
        try {
            System.out.println("GRAVANDDDDDDDDDDDDDDDDDDO");
            registerUser.registerUser(user);

        } catch (Exception e) {

            e.printStackTrace();
        }

    }**/


    private void createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.emptyFields).setPositiveButton(R.string.back, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialogEmpty = builder.create();
        alertDialogEmpty.show();
    }

    private void createDialogExist() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.emptyFields).setPositiveButton(R.string.back, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialogEmpty = builder.create();
        alertDialogEmpty.show();
    }


    public EditText getTxtEmail() {
        return txtEmail;
    }

    public void setTxtEmail(EditText txtEmail) {
        this.txtEmail = txtEmail;
    }

    public EditText getTxtUserName() {
        return txtUserName;
    }

    public void setTxtUserName(EditText txtUserName) {
        this.txtUserName = txtUserName;
    }

    public EditText getTxtPasswordRegister() {
        return txtPasswordRegister;
    }

    public void setTxtPasswordRegister(EditText txtPasswordRegister) {
        this.txtPasswordRegister = txtPasswordRegister;
    }

    public Button getBtnRegister() {
        return btnRegister;
    }

    public void setBtnRegister(Button btnRegister) {
        this.btnRegister = btnRegister;
    }

    public boolean isRegistered() {
        return isRegistered;
    }

    public void setIsRegistered(boolean isRegistered) {
        this.isRegistered = isRegistered;
    }

}
